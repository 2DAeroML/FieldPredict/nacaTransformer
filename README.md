# nacaTransformer

`nacaTransformer` is a novel transformer-based neural network designed to predict two-dimensional flow fields for the `airfoilMNIST` dataset.

## Getting Started

To train the model using custom parameters, a basic Docker image has been provided which can be used to execute the pipeline. For more details on how to run `docker` images and containers, please refer to the [Docker documentation](https://docs.docker.com/reference/).

The training parameters are stored in the `src/config.py`. 